FROM ubuntu:20.04

WORKDIR /app
ENV HOME /app

ENV TZ Asia/Hong_Kong
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

SHELL [ "/bin/bash", "-c" ]

RUN apt-get update && apt-get install curl sudo -y && \
    curl -sL https://raw.github.com/TheBastionBot/BastionScripts/main/linux/apt.sh -o BastionInstaller.sh && \
    alias reset="exit 0" && \
    . BastionInstaller.sh && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/

CMD /app/Bastion/bastion.sh --start && /app/Bastion/bastion.sh --show
